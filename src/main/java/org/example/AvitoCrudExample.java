package org.example;

import lombok.extern.slf4j.Slf4j;
import org.example.data.Flat;
import org.example.data.FlatSearch;
import org.example.exception.FlatIsNotFoundException;
import org.example.manager.FlatManager;

import java.util.List;

@Slf4j
public class AvitoCrudExample {
    public static void main(String[] args) {
        final FlatManager flatManager = new FlatManager();
        final Flat flatFirst = new Flat(0, 1, false, false, 20005000, 75, true, false, 3, 16);
        final Flat flatSecond = new Flat(0, 0, true, false, 3500000, 25, false, false, 3, 10);
        final Flat flatThird = new Flat(0, 2, false, false, 1500000, 48, false, false, 4, 10);
        final Flat flatFourth = new Flat(0, 2, false, false, 3500000, 50, false, false, 3, 8);
        final Flat flatFifth = new Flat(0, 1, false, false, 3500300, 75, true, false, 3, 16);

        log.debug("Creating 4 flats:");
        flatManager.create(flatFirst);
        flatManager.create(flatSecond);
        flatManager.create(flatThird);
        flatManager.create(flatFourth);

        flatManager.create(flatFifth);
        flatFifth.setId(flatFirst.getId());
        flatManager.update(flatFifth);
        log.debug("Flat with Id {} is updated", flatFirst.getId());

        log.debug("Flat with Id 2 is {}", flatManager.getById(2));

        flatManager.getCount();

        try {
            flatManager.removeById(1);
        } catch (FlatIsNotFoundException e) {
            log.error("Wrong Id");
        }
        try {
            flatManager.removeById(16);
        } catch (FlatIsNotFoundException e) {
            log.error("Wrong Id");
        }
        flatManager.getCount();

        FlatSearch flatSearchFirst = new FlatSearch(2, false, false, 1000000, 6000000, 1, 100, false, false, 1, 9, true, true, 1, 11);
        List<Flat> searchFirst = flatManager.search(flatSearchFirst);
        log.debug("Flats after filters (search1): {} ", searchFirst);

        FlatSearch flatSearchSecond = new FlatSearch(0, true, false, 1000000, 6000000, 20, 50, false, false, 2, 5, true, true, 1, 11);
        List<Flat> searchSecond = flatManager.search(flatSearchSecond);
        log.debug("Flats after filters (search2): {} ", searchSecond);
    }
}
